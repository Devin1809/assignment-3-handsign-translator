import { useContext } from "react"
import { useHistory } from "react-router"
import { LoginContext } from '../LoginContext'

function Profile () {

    const {loginInfo, setLoginInfo} = useContext(LoginContext)
    let history = useHistory()
    let translationsToDisplay = []

    if(loginInfo["username"] === undefined || loginInfo["username"] === null || loginInfo["username"] === '') {
        history.push('/')
    }

    for(const [index, value] of loginInfo["translations"].entries()) {
        translationsToDisplay.push(<li key={index}>{value}</li>)
    }

    function translatorEvent(){
        history.push('/translator')
    }

    function deleteEvent(){
        setLoginInfo({
            ...loginInfo,
            translationsTemp: '',
            translations: [],
            imageList: []
        })
        localStorage.removeItem('_translationsTemp');
        localStorage.removeItem('_translations');
        localStorage.removeItem('_imageList');
        history.push('/profile')
    }

    function logoutEvent(){
        setLoginInfo({
            ...loginInfo,
            username: '',
            translationsTemp: '',
            translations: [],
            imageList: []
        })
        localStorage.clear();
        history.push('/')
    }

    return (
        <main>
            <h3>Welcome {loginInfo["username"]}!</h3>
            <p>You can see your past translations here or logout</p>
            <p>This app stores the last 10 translation</p>

            <button id="translatorButton" onClick={ deleteEvent }>Delete Translations</button>
            <p></p>
            <div>
                { translationsToDisplay }
            </div>
            <p></p>
            <button className="butStyle" id="translatorButton" onClick={ translatorEvent }>Go to Translator</button>
            <button className="butStyle" id="logoutButton" onClick={ logoutEvent }>Logout</button>

        </main>
    )
}

export default Profile;