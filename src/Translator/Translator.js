import { React, useContext } from "react"
import { useHistory } from "react-router"
import { LoginContext } from '../LoginContext'
import images from "../assets/signs"

function Translator () {

    const {loginInfo, setLoginInfo} = useContext(LoginContext)
    let history = useHistory()

    let imageList = loginInfo["imageList"]
    let translationsToDisplay = []

    localStorage.setItem('_username', loginInfo["username"])

    if(loginInfo["username"] === undefined || loginInfo["username"] === null || loginInfo["username"] === '') {
        history.push('/')
    }

    function onTranslationChange(e) {
        setLoginInfo({
            ...loginInfo,
            translationsTemp: e.target.value.toLowerCase().trim(),
            imageList: ''
        })
    }

    function storeTranslation() {
        if(loginInfo["translationsTemp"] === "") {
            return
        }
        if(loginInfo["translations"].length >= 10)
        {
            setLoginInfo({
                ...loginInfo,
                translations: loginInfo["translations"].shift()
            })
        }

        setLoginInfo({
            ...loginInfo,
            translations: loginInfo["translations"].push(loginInfo["translationsTemp"])
        })
        localStorage.setItem('_translations', loginInfo["translations"])
        console.log(JSON.stringify(loginInfo["translations"]))

        showImages()
    }

    function showImages(){
        //Turns the word to translate info a list of indexes
        let translationLetters = loginInfo["translationsTemp"]
        imageList = []

        for(let i = 0; i < translationLetters.length; i++) {
            imageList.push(images[translationLetters[i]])
        } 
    
        for(const [index, value] of imageList.entries()) {
            translationsToDisplay.push(<img className="photo" key={index} src={value} alt={images[26]}/>)
        }

        setLoginInfo({
            ...loginInfo,
            imageList: translationsToDisplay
        })
    }

    function profileEvent(){
        setLoginInfo({
            ...loginInfo,
            translationsTemp: ''
        })        
        history.push('/profile')
    }


    return (
        <main>
            <h3>Welcome {loginInfo["username"]}!</h3>
            <p>You can translate words here!</p>
            <p>It will return sign language</p>

            <label htmlFor="translationsTemp">Word to translate</label>
            <p></p>
            <input type="text" id="translationsTemp" onChange={ onTranslationChange }></input>
            <button id="translationButton" onClick={ storeTranslation }>Translate</button>
            <p></p>
            <div className="translationBox">
                { loginInfo["translationsTemp"]}
                <p></p>
                { loginInfo["imageList"] }
            </div>
            <p></p>
            { /*
                <img src={images[25]} alt="z"/>
            */   }
            <p></p>
            <button className="butStyle" id="profileButton" onClick={ profileEvent }>Go to Profile</button>

        </main>
    )
}

export default Translator;