import './App.css'
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from 'react-router-dom'
import React, { useState } from 'react'
import { LoginContext } from './LoginContext'

import Login from './Login/Login'
import Translator from './Translator/Translator'
import Profile from './Profile/Profile'
import NotFound from './NotFound/NotFound'

function App() { 

//This splits the stored translations into an array if it's not empty
let translationCheck = localStorage.getItem('_translations')
if(translationCheck !== null && translationCheck !== undefined) {
  translationCheck = localStorage.getItem('_translations').split(",")
}
else {
  translationCheck = []
}

let usernameCheck = localStorage.getItem('_username')
if(usernameCheck !== null && usernameCheck !== undefined) {
  usernameCheck = localStorage.getItem('_username')
}
else {
  usernameCheck = ''
}

const [loginInfo, setLoginInfo] = useState({
  usernameTemp: '',
  username: usernameCheck,
  translationsTemp: '',
  translations: translationCheck,
  imageList: ''
}) 

  return (
    <BrowserRouter>
      <div className="App">
      <header>
        <h1>Lost in Translation</h1>
      </header>
      <LoginContext.Provider value={{ loginInfo, setLoginInfo }}>
        <Switch>
          <Route path='/' exact component={ Login }/>
          <Route path='/translator' component={ Translator }/>
          <Route path='/profile' component={ Profile }/>
          <Route path='*' component={ NotFound }/>
        </Switch>
      </LoginContext.Provider>
      </div>
    </BrowserRouter>
  );
}

export default App;
