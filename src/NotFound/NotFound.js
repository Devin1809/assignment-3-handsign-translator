import { useContext } from "react";
import { LoginContext } from '../LoginContext';
import { Link } from 'react-router-dom';

function NoPath () {

    const {loginInfo, setLoginInfo} = useContext(LoginContext)

    return (
        <main>
            <h4> Oops!</h4>
            <p>This page doesn't exist :(</p>
            <Link to='/'>Go to homepage</Link>
        </main>
    )
}

export default NoPath;
