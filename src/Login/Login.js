import { useContext } from "react"
import { useHistory } from "react-router"
import { LoginContext } from '../LoginContext'

function Login () {

    const {loginInfo, setLoginInfo} = useContext(LoginContext)
    let history = useHistory()

    if(loginInfo["username"] !== undefined && loginInfo["username"] !== null && loginInfo["username"] !== '') {
        history.push('/translator')
    }

    function onUserChange(e) {
        setLoginInfo({
            ...loginInfo,
            usernameTemp: e.target.value.trim()
        })
        console.log(e.target.value)
        console.log(loginInfo)
    } 
    
    function onUserSave() {
        if(loginInfo["usernameTemp"] === "") {
            return
        }
        setLoginInfo({
            ...loginInfo,
            username: loginInfo["usernameTemp"],
            usernameTemp: ''
        })
        history.push('/translator')
    }

    return (
        <main>
            <h4> This will ask you to enter a username</h4>
            <p>Then you can start using the translator</p>

            <form className="mt-3 mb-3">
                <div className="mb-3">
                    <label htmlFor="username" className="form-label">Username</label>
                    <input id="username" type="text" placeholder="enter a username"
                        className="form-control" onChange={ onUserChange }/>
                    <p></p>
                    <button htmlFor="username" type="button" className="btn-lg" onClick={ onUserSave }>Login</button>
                </div>


            </form>

        </main>
    )
}

export default Login;
